var express       = require('express');
var path          = require('path');
var logger        = require('morgan');
var bodyParser    = require('body-parser');
var mongoose      = require('mongoose');
var passport	  = require('passport');
var session		  = require('express-session');
var config        = require('./config');

mongoose.connect('mongodb://root:abc123@ds041238.mongolab.com:41238/twitterauth', function() {
  console.log("Connected to the database");
})

require('./passport')(passport);

var app = express();

var authenticate  = require('./routes/authenticate')(app, express, passport);
var api           = require('./routes/api') (app, express, passport);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// I need to add this in order to make twitter authentication working properly
app.use(session({
	secret: config.TOKEN_SECRET,
	resave: true,
    saveUninitialized: true,
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use('/auth', authenticate);
app.use('/api', api);

app.get('*', function(req, res) {
	res.sendFile(__dirname + '/public/app/views/index.html');
});


app.listen(3000, function(err) {
  if(err) {
    return res.send(err);
  }
  console.log("Listening on port 3000");
});