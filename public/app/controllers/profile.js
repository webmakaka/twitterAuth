angular.module('MyApp')
  .controller('ProfileCtrl', function($scope, $auth, Account) {
      Account.getProfile()
        .success(function(data) {
          $scope.user = data;
        });
  });